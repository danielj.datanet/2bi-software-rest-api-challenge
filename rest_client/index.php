<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Rest Country API Client</title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />
		<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<style type="text/css">
			.text-bold{
				font-weight: bold;
			}
		</style>
		

	</head>

	<body class="no-skin">
		<div class="main-container ace-save-state" id="main-container">
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<p class="align-center">Rest Countries API Client</p>
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-sm-4"></div>
									<div class="col-sm-8">
										<form class="form-search" method="post">
											<div class="row">
												<div class="col-xs-12 col-sm-6">
													<div class="input-group input-group-lg">
														<input type="text" class="form-control search-query" placeholder="Digite o nome do pais" id="country_name" required="" />
														<span class="input-group-btn">
															<button type="button" class="btn btn-default btn-lg" onclick="searchCountry()">
																<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																Pesquisar
															</button>
														</span>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4"></div>
									<div class="col-sm-4">
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="smaller align-center">
													Dados do país
													
												</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<div id="info-container">
											     <p>Nome: <span id="nome" class="text-bold"></span></p>
											<p>Nome nativo: <span id="nome-nativo" class="text-bold"></span></p>
											<p>Capital: <span id="capital" class="text-bold"></span></p>
											<p>População: <span id="populacao" class="text-bold"></span></p>
											<p>Area: <span id="area" class="text-bold"></span></p>
										<p>Fuso Horário: <span id="fuso-horario" class="text-bold"></span></p>
											 <p>Região: <span id="regiao" class="text-bold"></span></p>
											 <p>Sub Região: <span id="sub-regiao" class="text-bold"></span></p>
											 <p>Bandeira: <span class="text-bold"><a href="#" id="bandeira" target="_blank">Clique aqui para ver a bandeira</a></span></p>
											    </div>

													<hr>
														<p class="text-uppercase">Exportar dados</p>
													<p>
														<span class="btn btn-sm" data-rel="tooltip" title="" data-original-title="CSV" onclick="exportToCSV()">CSV</span>
														<span class="btn btn-warning btn-sm tooltip-warning" data-rel="tooltip" data-placement="left" title="" data-original-title="XLS" onclick="exportToXLS()">XLS</span>
														<span class="btn btn-success btn-sm tooltip-success" data-rel="tooltip" data-placement="right" title="" data-original-title="XML" onclick="exportToXML()">XML</span>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript">
			var control = 0;
			function searchCountry() {
				var nome = document.getElementById("country_name").value;
				if(nome == ""){
					alert( "Por favor verifica se digitou correctamente o nome ou sigla do país");
				}else{
				var apiEndpoint = "https://restcountries.eu/rest/v2/name/".concat(nome);
				var flagId = document.getElementById("bandeira");

				 var apiResponse =$.get(apiEndpoint, function() {
				 //console.log(apiResponse);
				 control = 1;
				  document.getElementById("nome").innerHTML = 
				  	apiResponse.responseJSON[0].name;
			  	  document.getElementById("nome-nativo").innerHTML = 
			  	  	apiResponse.responseJSON[0].nativeName;
			      document.getElementById("capital").innerHTML = 
			      	apiResponse.responseJSON[0].capital;
			      document.getElementById("populacao").innerHTML =
			      	apiResponse.responseJSON[0].population.toLocaleString("en-US");
			      document.getElementById("area").innerHTML =
			      	apiResponse.responseJSON[0].area.toLocaleString("en-US").concat(' km²');
			      document.getElementById("fuso-horario").innerHTML =
			      	apiResponse.responseJSON[0].timezones;
			      document.getElementById("regiao").innerHTML =
			      	apiResponse.responseJSON[0].region;
			      document.getElementById("sub-regiao").innerHTML =
			      	apiResponse.responseJSON[0].subregion;
			      flagId.setAttribute('href', apiResponse.responseJSON[0].flag);
				})
				  .fail(function() {
				    alert( "Ocorreu um erro, Verifica se digitou correctamento o nome.");
				  });
				}
			}

			function exportToCSV(){
				var nome = document.getElementById("country_name").value;
				if(nome == "" || control ==0){
				 	alert( "Por favor verifica se digitou correctamente o nome ou a sigla do país");
				 }else{
				 	 window.location.href="controller/cExport.php?country="+nome+"&file_type=csv";
				 }				
			}
			function exportToXLS(){
				var nome = document.getElementById("country_name").value;
				 if(nome == "" || control ==0){
				 	alert( "Por favor verifica se digitou correctamente o nome ou a sigla do país");
				 }else{
				 	window.location.href="controller/cExport.php?country="+nome+"&file_type=xls";
				 }
			}
			function exportToXML(){
				var nome = document.getElementById("country_name").value;
				if(nome == "" || control ==0){
				 	alert( "Por favor verifica se digitou correctamente o nome ou a sigla do país");
				 }else{
				 	window.location.href="controller/cExport.php?country="+nome+"&file_type=xml";
				 }				 
			}
		</script>
	</body>
</html>
