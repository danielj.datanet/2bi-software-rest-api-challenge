<?php
        $country = $_GET['country'];
        $file_type = $_GET['file_type'];

        if(isset($_GET['country'])){
           // https://restcountries.eu/rest/v2/all
        $url = "https://restcountries.eu/rest/v2/name/".$country;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $resultado = json_decode(curl_exec($ch));

        $someDetails = array(
                array('Nome','Nome nativo','Capital','Populacao','Area','Fuso Horario','Regiao','Sub Regiao','Link da Bandeira'),
                array($resultado[0]->name,$resultado[0]->nativeName,$resultado[0]->capital,$resultado[0]->population,$resultado[0]->area,$resultado[0]->timezones[0],$resultado[0]->region,$resultado[0]->subregion,$resultado[0]->flag)
            );

        if($file_type =="csv"){
            header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=".$resultado[0]->name.".csv");
            $fp = fopen("php://output", 'w');
            foreach($someDetails as $row){
                
                fputcsv($fp, $row);
            } 
            fclose($fp);
        }elseif($file_type == "xml"){
            $xml ='<?xml version="1.0" encoding="ISO-8859-1"?>';
            $xml .= '<country_details>';
                $xml .= '<nome>' . $someDetails[1][0]. '</nome>';
                $xml .= '<nome_nativo>' . $someDetails[1][1]. '</nome_nativo>';
                $xml .= '<capital>' . $someDetails[1][2]. '</capital>';
                $xml .= '<populacao>' . $someDetails[1][3]. '</populacao>';
                $xml .= '<area>' . $someDetails[1][4]. '</area>';
                $xml .= '<fuso_horario>' . $someDetails[1][5]. '</fuso_horario>';
                $xml .= '<regiao>' . $someDetails[1][6]. '</regiao>';
                $xml .= '<sub_regiao>' . $someDetails[1][7]. '</sub_regiao>';
                $xml .= '<bandeira>' . $someDetails[1][8]. '</bandeira>';
            $xml .= '</country_details>';

            header("Content-type: application/xml");
            header("Content-Disposition: attachment; filename=".$someDetails[1][0].".xml");
            $fp = fopen("php://output", 'w+');
            fwrite($fp, $xml);
            fclose($fp);
        }else{
            $tabela ='
                <table border="1">
                    <tr>
                        <th>Nome</th>
                        <th>Nome nativo</th>
                        <th>Capital</th>
                        <th>Populacao</th>
                        <th>Area</th>
                        <th>Fuso Horario</th>
                        <th>Regiao</th>
                        <th>Sub Regiao</th>
                        <th>Link da bandeira</th>
                    </tr>
                    <tr>
                        <td>'.$someDetails[1][0].'</td>
                        <td>'.$someDetails[1][1].'</td>
                        <td>'.$someDetails[1][2].'</td>
                        <td>'.$someDetails[1][3].'</td>
                        <td>'.$someDetails[1][4].'</td>
                        <td>'.$someDetails[1][5].'</td>
                        <td>'.$someDetails[1][6].'</td>
                        <td>'.$someDetails[1][7].'</td>
                        <td>'.$someDetails[1][8].'</td>
                    </tr>
                </table>
                ';
            $arquivo = "Dados de ".$someDetails[1][0].".xls";

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$arquivo.'"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
               
            echo $tabela;  
            exit;

        }
        
    }