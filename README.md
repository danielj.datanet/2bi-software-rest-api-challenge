#2Bi Software Rest Countries API Challenge

Este desafio permite visualizar e exportar os dados de um a pais com ajuda da API Rest Countries, foi desenvolvido usando as seguintes linguagens: #HTML, #CSS, #JavaScript e #PHP

#COMO USAR
Para usar é necessario fornecer o nome ou a sigla do país (Ex: Zimbabwe/Zim/ZW, Moçambique/Moz/MZ) e de seguida clicar no botao "Pesquisar".
Apos a busca, os dados serão mostrados e podem ser exportados para XML, CSV e XLS

#SOBRE O AUTOR
Daniel Jemusse Massandudze, formado em Engenharia Informática, Programador Web
danielmasandudzi@gmail.com